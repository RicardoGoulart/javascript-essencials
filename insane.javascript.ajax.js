/*!
 * InsaneIntegration v1.1 $insane
 * Copyright 2010-2018 Ricardo Goulart
 * Licensed under the GNU license
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 */

if( $insane == void 0 ){
  throw "Before using $insane, you need to include the insane.boot.js file first.";
}

/*
 ____ ____ ____ ____ 
||A |||J |||A |||X ||
||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|

*/

$insane.ajax = {};

$insane.ajax.request = function( u, s, c, cd, m, dt, t, d, fs, fe, fc ){
  var jQ = null;
  if( window.$ != void 0 ){
    jQ = $;
  } else if( window.jQuery != void 0 ){
    jQ = jQuery;
  } else {
    throw '$insane ajax needs jQuery.';
  }

  if( u == void 0 ){
    throw 'Missing argument URL (1)';
  }

  var request = JQ.ajax({
    url         : u,
    async       : s || true,
    cache       : c || true,
    crossDomain : cd || false,
    dataType    : dt || undefined,
    method      : m || 'GET',
    timeout     : t || 45,

    data        : d || {},

    success     : fs || function(){},
    error       : fe || function(){},
    complete    : fc || function(){}
  });

  return request || false;
};

$insane.ajax.get = {};

$insane.ajax.get.html = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'GET', 'html', t, d, fs, fe, fc );
};

$insane.ajax.get.json = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'GET', 'json', t, d, fs, fe, fc );
};

$insane.ajax.get.script = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'GET', 'script', t, d, fs, fe, fc );
};

$insane.ajax.get.xml = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'GET', 'xml', t, d, fs, fe, fc );
};

$insane.ajax.post = {};

$insane.ajax.post.html = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'POST', 'html', t, d, fs, fe, fc );
};

$insane.ajax.post.json = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'POST', 'json', t, d, fs, fe, fc );
};

$insane.ajax.post.script = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'POST', 'script', t, d, fs, fe, fc );
};

$insane.ajax.post.xml = function( u, s, c, cd, d, fs, fe, fc, t ){
  return $insane.ajax.request( u, s, c, cd, 'POST', 'xml', t, d, fs, fe, fc );
};