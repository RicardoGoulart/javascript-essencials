/*!
 * InsaneIntegration v1.1 $insane
 * Copyright 2010-2018 Ricardo Goulart
 * Licensed under the GNU license
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 */

if( $insane == void 0 ){
  throw "Before using $insane, you need to include the insane.boot.js file first.";
}

/*
 ____ ____ ____ ____ ____ ____ 
||I |||M |||A |||G |||E |||S ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.image                           = {};

$insane.image.config                    = {};

$insane.image.config.waitTime           = 2500;
$insane.image.config.errorReporing      = true;
$insane.image.config.minPixel           = 5;

$insane.image.config.classNames         = {};

$insane.image.config.classNames.before  = "_INIB_";
$insane.image.config.classNames.after   = "_INIA_";
$insane.image.config.classNames.loading = "_INILOADING_";

$insane.image.config.attributes         = {};

$insane.image.config.attributes.low     = "insane-attr-img-low";
$insane.image.config.attributes.high    = "insane-attr-img-high";
$insane.image.config.attributes.method  = "insane-attr-img-method";
$insane.image.config.attributes.quality = "insane-attr-img-quality";
$insane.image.config.attributes.parsed  = "insane-attr-img-parsed";
$insane.image.config.attributes.source  = "insane-attr-img-source";
$insane.image.config.attributes.id      = "insane-attr-img-id";

$insane.image.status                    = {};

$insane.image.status.started            = false;
$insane.image.status.intervalIndex      = 0;

$insane.image.elements                  = {};

$insane.image.elements.loader           = null;
$insane.image.elements.default          = null;
$insane.image.elements.loaders          = [];
$insane.image.elements.containers       = [];

  if( window.location.origin != void 0 ){
    $insane.image.config.classNames.before  += window.location.origin;
    $insane.image.config.classNames.after   += window.location.origin;
    $insane.image.config.classNames.loading += window.location.origin;
  } else {
    $insane.image.config.classNames.before  += 'No_browser_support';
    $insane.image.config.classNames.after   += 'No_browser_support';
    $insane.image.config.classNames.loading += 'No_browser_support';
  }

$insane.image.config.classNames.before  += $insane.version;
$insane.image.config.classNames.after   += $insane.version;
$insane.image.config.classNames.loading += $insane.version;

  if( $insane.encode.md5 != void 0 ){
    $insane.image.config.classNames.before  = $insane.encode.md5( $insane.image.config.classNames.before );
    $insane.image.config.classNames.after   = $insane.encode.md5( $insane.image.config.classNames.after );
    $insane.image.config.classNames.loading = $insane.encode.md5( $insane.image.config.classNames.loading );
  } else { 
    $insane.image.config.classNames.before  = $insane.string.slugify( window.atob( $insane.image.config.classNames.before ) );
    $insane.image.config.classNames.after   = $insane.string.slugify( window.atob( $insane.image.config.classNames.after ) );
    $insane.image.config.classNames.loading = $insane.string.slugify( window.atob( $insane.image.config.classNames.loading ) );
  }

$insane.image.showErrors = function( v ){
  if( v != void 0 ){
    $insane.image.config.errorReporing = v;
  } else {
    return $insane.image.config.errorReporing;
  }
};

$insane.image.register = {};

$insane.image.register.default = function( u ){
  if( u !== void 0 && typeof u == "string" ){
    $insane.image.elements.default = u;
  } else {
    return false;
  }
};

$insane.image.register.loader = function( u ){
  if( u != void 0 ){
    if( u instanceof Array ){
      for( var i = 0, m = u.length; i < m; i++ ){
        $insane.image.elements.loaders.push( u[i] );
      }
    } else if( typeof u == "string" ){
      $insane.image.elements.loaders.push( u );
    } else {
      $insane.log( "Provided argument is neither a string or an array." );
      return false;
    }
    return $insane.image.elements.loaders;
  } else {
    return $insane.image.elements.loaders;
  }
};

$insane.image.register.container = function( c ){
  if( c != void 0 ){
    if( c instanceof Array ){
      for( var i = 0, m = c.length; i < m; i++ ){
        $insane.image.elements.containers.push( c[i] );
      }
    } else {
      $insane.image.elements.containers.push( c );
    }
    return $insane.image.elements.containers;
  } else {
    return false;
  }
};

$insane.image.execute = {};

$insane.image.execute.init = function(){
  try {
    var jQ = null;
    if( window.$ != void 0 ){
      jQ = $;
    } else if( window.jQuery != void 0 ){
      jQ = jQuery;
    } else {
      throw '$insane image needs jQuery to handle DOM elements.';
    }

    if( $insane.image.status.started === true ){
      throw "Instance was already started.";
    } else {
      $insane.image.status.started = true;
    }

    if( !$insane.image.elements.default ){
      $insane.image.status.started = false;
      throw new Error( "Cannot start without fail-safe image registered. Please use: $insane.image.register.default( imagePath String ) and try again." );
    }

    if( ! $insane.image.elements.loaders instanceof Array ){
      $insane.image.status.started = false;
      throw new Error( "You should use $insane.image.register.loader() function instead of editing the element directly." );
    }

    if( $insane.image.elements.loaders.length === 0 ){
      $insane.image.status.started = false;
      throw new Error( "Cannot start without at least ONE loader registered. Please use: $insane.image.register.loader( imagePath String ) and try again." );
    }

    if( $insane.image.elements.containers.length === 0 ){
      $insane.image.status.started = false;
      throw new Error( "Cannot start without at least ONE container registered. Please use: $insane.image.register.container( [body, window, #div] ) and try again." );
    }

    /* Create DOM style */
    jQ( 'head' ).append( '<style type="text/css">img.' + $insane.image.config.classNames.loading + ' { position: relative; width: auto; display: block; margin: 0 auto; text-align: center; }</style>' );

    $insane.document.ready( function(){
      setTimeout( function(){
        $insane.image.execute.parse();
      }, $insane.image.config.waitTime );
    });

    $insane.image.apply.attributes();

    jQ( window ).resize( $insane.image.execute.parse );
    jQ( window ).scroll( $insane.image.execute.parse );

    for( var i = 0, m = $insane.image.elements.containers.length; i < m; i++ ){
      jQ( $insane.image.elements.containers[i] ).on( 'scroll click', $insane.image.execute.parse );
    }

    $insane.image.execute.parse();
  } catch( e ){
    $insane.log( e.message );
    return false;
  }
};

$insane.image.execute.parse = function(){
  $insane.log( "Parsing images..." );

  if( $insane.image.status.started !== true ){
    $insane.log( "You need to start with $insane.image.execute.init() first!" );
    return false;
  }

  var jQ = null;
  if( window.$ != void 0 ){
    jQ = $;
  } else if( window.jQuery != void 0 ){
    jQ = jQuery;
  } else {
    throw '$insane image needs jQuery to handle DOM elements.';
  }

  var imageId             = null;
  var imageLoader         = [];
  var imageContainer      = [];
  var imageHandler        = [];
  var imageParent         = null;
  var imageCalcWidth      = null;
  var imageCalcHeight     = null;
  var imageConfirmedWidth = null;
  var imageInsaneMethod   = null;
  var imageInsaneQuality  = null;
  var imageInsaneSource   = null;
  var imageInsaneParams   = null;

  jQ( "img[" + $insane.image.config.attributes.source + "]:visible" ).each( function(){
    imageId                 = jQ( this ).attr( $insane.image.config.attributes.id );
    imageContainer[imageId] = jQ( this );    
    imageParent             = jQ( this ).parent();
    imageCalcWidth          = Math.floor( imageParent.innerWidth() );
    imageCalcHeight         = Math.floor( imageParent.innerHeight() );
    imageConfirmedWidth     = imageCalcWidth >= $insane.image.config.minPixel;
    imageInsaneMethod       = jQ( this ).attr( $insane.image.config.attributes.method ) || false;
    imageInsaneQuality      = jQ( this ).attr( $insane.image.config.attributes.quality ) || 90;
    imageInsaneSource       = jQ( this ).attr( $insane.image.config.attributes.source ) || false;
    
    if( imageConfirmedWidth && imageInsaneSource && imageInsaneMethod ){
      if( !jQ( this ).attr( $insane.image.config.attributes.low ) ){
        imageInsaneParams = {
          'url'           : imageInsaneSource,
          'method'        : imageInsaneMethod,
          'quality'       : 1,
          'calcWidth'     : imageCalcWidth,
          'calcHeight'    : imageCalcHeight
        };
        jQ( this ).attr( $insane.image.config.attributes.low, imageInsaneSource + "/" + $insane.encode.insane( imageInsaneParams ) )
      }

      imageInsaneParams = {
        'url'           : imageInsaneSource,
        'method'        : imageInsaneMethod,
        'quality'       : imageInsaneQuality,
        'calcWidth'     : imageCalcWidth,
        'calcHeight'    : imageCalcHeight
      };
      jQ( this ).attr( $insane.image.config.attributes.high, imageInsaneSource + "/" + $insane.encode.insane( imageInsaneParams ) )
    }

    imageHandler[imageId] = function( g ){
      switch( jQ( g ).attr( $insane.image.config.attributes.parsed ).toLowerCase() ){
        case 'low':
          if( jQ( g ).attr( $insane.image.config.attributes.high ) ){
            if( typeof( imageLoader[imageId] ) == "undefined" ){
              imageLoader[imageId] = new Image();
              imageLoader[imageId].onload = function(){
                jQ( g ).prop( "src", this.src );
                jQ( g ).removeClass( $insane.image.config.classNames.loading );
                jQ( g ).attr( $insane.image.config.attributes.parsed, 'high' );
                imageLoader[imageId] = undefined;
                delete imageLoader[imageId];
                imageHandler[imageId]( g );
              };
              imageLoader[imageId].onerror = function(){
                this.src = $insane.image.elements.default;
              };
              imageLoader[imageId].src = jQ( g ).attr( $insane.image.config.attributes.high );
            }
          }
        break;
  
        case 'high':
          if( jQ( g ).attr( $insane.image.config.attributes.high ) ){
            if( typeof( imageLoader[imageId] ) == "undefined" ){
              imageLoader[imageId] = new Image();
              imageLoader[imageId].onload = function(){
                jQ( g ).prop( "src", this.src );
                jQ( g ).attr( $insane.image.config.attributes.parsed, 'high' );
                imageLoader[imageId] = undefined;
                delete imageLoader[imageId];
              };
              imageLoader[imageId].onerror = function(){
                this.src = $insane.image.elements.default;
              };              
              if( jQ( g ).prop( "src" ) != $insane.image.config.attributes.high ){
                imageLoader[imageId].src = jQ( g ).attr( $insane.image.config.attributes.high );
              }
            }
          }
        break;
  
        default:
        case 'none':
          if( jQ( g ).attr( $insane.image.config.attributes.low ) ){
            if( typeof( imageLoader[imageId] ) == "undefined" ){
              imageLoader[imageId] = new Image();
              imageLoader[imageId].onload = function(){
                jQ( g ).prop( "src", this.src );
                jQ( g ).attr( $insane.image.config.attributes.parsed, 'low' );
                jQ( g ).removeClass( $insane.image.config.classNames.before );
                jQ( g ).addClass( $insane.image.config.classNames.after );
                imageLoader[imageId] = undefined;
                delete imageLoader[imageId];
                imageHandler[imageId]( g );
              };
              imageLoader[imageId].onerror = function(){
                this.src = $insane.image.elements.default;
              };
              imageLoader[imageId].src = jQ( g ).attr( $insane.image.config.attributes.low );
            }
          } else {
            if( typeof( imageLoader[imageId] ) == "undefined" ){
              imageLoader[imageId] = new Image();
              imageLoader[imageId].onload = function(){
                jQ( g ).prop( "src", this.src );
                jQ( g ).attr( $insane.image.config.attributes.parsed, 'high' );
                jQ( g ).removeClass( $insane.image.config.classNames.loading );
                jQ( g ).removeClass( $insane.image.config.classNames.before );
                jQ( g ).addClass( $insane.image.config.classNames.after );
                imageLoader[imageId] = undefined;
                delete imageLoader[imageId];
                imageHandler[imageId]( g );
              };
              imageLoader[imageId].onerror = function(){
                this.src = $insane.image.elements.default;
              };
              imageLoader[imageId].src = jQ( g ).attr( $insane.image.config.attributes.high );
            }
          }
        break;
      }
    };

    imageHandler[imageId]( this );

  });
};

$insane.image.apply = {};

$insane.image.apply.default = function( o ){
  if( $insane.image.status.started !== true ){
    $insane.log( "You need to start with $insane.image.execute.init() first!" );
    return false;
  }

  var jQ = null;
  if( window.$ != void 0 ){
    jQ = $;
  } else if( window.jQuery != void 0 ){
    jQ = jQuery;
  } else {
    throw '$insane image needs jQuery to handle DOM elements.';
  }

  if( $insane.image.config.errorReporing === true ){
    $insane.log( "$insane Image failed to load: ", o );
  }

  jQ( o ).prop( "src", $insane.image.elements.default );
};

$insane.image.apply.attributes = function(){
  if( $insane.image.status.started !== true ){
    $insane.log( "You need to start with $insane.image.execute.init() first!" );
    return false;
  }

  var jQ = null;
  if( window.$ != void 0 ){
    jQ = $;
  } else if( window.jQuery != void 0 ){
    jQ = jQuery;
  } else {
    throw '$insane image needs jQuery to handle DOM elements.';
  }

  $insane.image.elements.loader = $insane.image.elements.loaders[Math.abs( Math.floor( Math.random() * $insane.image.elements.loaders.length ) )];

  for( var i = 0, m = $insane.image.elements.containers.length; i < m; i++ ){
    jQ( $insane.image.elements.containers[i] + ' img[src]:not( .' + $insane.image.config.classNames.before + ', .' + $insane.image.config.classNames.after + ' )' ).each( function( k, v ){
      jQ( this ).attr( "onerror", "$insane.image.apply.default( this );" );
      if( !jQ( this ).attr( $insane.image.config.attributes.parsed ) ){
        jQ( this ).attr( $insane.image.config.attributes.parsed, "none" );
        jQ( this ).attr( $insane.image.config.attributes.source, jQ( this ).prop( "src" ) );
        jQ( this ).prop( "src", $insane.image.elements.loader );
        jQ( this ).attr( $insane.image.config.attributes.id, $insane.encode.md5( $insane.document.getXPath( jQ( this )[0] ) ) );

        if( !jQ( this ).attr( $insane.image.config.attributes.low ) ){
          jQ( this ).attr( $insane.image.config.attributes.low, jQ( this ).attr( $insane.image.config.attributes.source ) );
        }

        if( !jQ( this ).attr( $insane.image.config.attributes.high ) ){
          jQ( this ).attr( $insane.image.config.attributes.high, jQ( this ).attr( $insane.image.config.attributes.source ) );
        }

        jQ( this ).addClass( $insane.image.config.classNames.loading );
        jQ( this ).addClass( $insane.image.config.classNames.before );
      }
    });
  }
};