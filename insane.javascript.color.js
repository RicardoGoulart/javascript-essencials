/*!
 * InsaneIntegration v1.1 $insane
 * Copyright 2010-2018 Ricardo Goulart
 * Licensed under the GNU license
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 */

if( $insane == void 0 ){
  throw "Before using $insane, you need to include the insane.boot.js file first.";
}

/*
 ____ ____ ____ ____ ____ ____ 
||C |||O |||L |||O |||R |||S ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.color     = {};

$insane.color.darken = function( c, a ){
  var color     = c || false;
  var amount    = a || false;
  var usePound  = false;
  var num       = 0;
  var r         = 0;
  var b         = 0;
  var g         = 0;

  if( color && amount ){
    amount = -Math.abs( amount );
    
    if( color[0] == "#" ){
      color = color.slice( 1 );
      usePound = true;
    }

    num = parseInt( color, 16 );
    r = ( num >> 16 ) + amount;

    if( r > 255 ){
      r = 255;
    } else if( r < 0 ){
      r = 0;
    }

    b = ( ( num >> 8 ) & 0x00FF ) + amount;

    if( b > 255 ){
      b = 255;
    } else if( b < 0 ){
      b = 0;
    }

    g = ( num & 0x0000FF ) + amount;

    if( g > 255 ){
      g = 255;
    } else if( g < 0 ){
      g = 0;
    }

    return ( usePound ? "#" : "" ) + ( g | ( b << 8 ) | ( r << 16 ) ).toString( 16 );
  } else {
    $insane.log( 'Darken needs to receive both parameters! Color (in HEX) and amount (int).' );
    return 0;
  }
};

$insane.color.lighten = function( c, a ){
  var color     = c || false;
  var amount    = a || false;
  var usePound  = false;
  var num       = 0;
  var r         = 0;
  var b         = 0;
  var g         = 0;

  if( color && amount ){
    amount = Math.abs( amount );
    
    if( color[0] == "#" ){
      color = color.slice( 1 );
      usePound = true;
    }

    num = parseInt( color, 16 );
    r = ( num >> 16 ) + amount;

    if( r > 255 ){
      r = 255;
    } else if( r < 0 ){
      r = 0;
    }

    b = ( ( num >> 8 ) & 0x00FF ) + amount;

    if( b > 255 ){
      b = 255;
    } else if( b < 0 ){
      b = 0;
    }

    g = ( num & 0x0000FF ) + amount;

    if( g > 255 ){
      g = 255;
    } else if( g < 0 ){
      g = 0;
    }

    return ( usePound ? "#" : "" ) + ( g | ( b << 8 ) | ( r << 16 ) ).toString( 16 );
  } else {
    $insane.log( 'Darken needs to receive both parameters! Color (in HEX) and amount (int).' );
    return 0;
  }
}

$insane.color.HEX2RGB = function( s, t ){
  /* Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF") */
  var t       = t || false;
  var regEx1  = new RegExp( "^#?([a-f\\d])([a-f\\d])([a-f\\d])$", "i" );
  var hex     = s.replace( regEx1, function( m, r, g, b ){
    return r + r + g + g + b + b;
  });
  var regEx2  = new RegExp( "^#?([a-f\\d]{2})([a-f\\d]{2})([a-f\\d]{2})$", "i" );
  var result  = regEx2.exec( hex );
  var returnResult =  result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  } : null;
  
  if( returnResult ){
    switch( t ){
      case 'array':
        return [ parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16) ];
      break;
      case 'object':
      case true:
        return returnResult;
      break;
      default:
      case 'string':
      case false:
        return "rgb( " + parseInt(result[1], 16) + "," + parseInt(result[2], 16) + "," + parseInt(result[3], 16) + " )";
      break;
    }
  } else {
    return null;
  }
};

$insane.color.RGB2HEX = function( r, g, b ){
  if( r != void 0 && g == void 0 && b == void 0 ){
    /* Limpar RGB caso exista.. */
    if( typeof( r ) == "object" ){
      if( r.hasOwnProperty( "r" ) && r.hasOwnProperty( "g" ) && r.hasOwnProperty( "g" ) ){
        b = r.b;
        g = r.g;
        r = r.r;
      }
    }

    if( typeof( r ) == "string" ){
      if( r.indexOf( "(" ) > -1 ){
        r = r.replace( "(", "" ).replace( ")", "" );
      }

      if( r.indexOf( "rgba" ) > -1 ){
        r = r.replace( "rgba", "" );
      }

      if( r.indexOf( "rgb" ) > -1 ){
        r = r.replace( "rgb", "" );
      }

      r = r.split( "," );
      b = r[2];
      g = r[1];
      r = r[0];
    }

    if( r instanceof Array ){
      if( r.length === 3 ){
        b = r[2];
        g = r[1];
        r = r[0];
      }
    }
  }

  if( r != void 0 && g != void 0 && b != void 0 ){
    r = parseInt( r );
    g = parseInt( g );
    b = parseInt( b );
  }

  var rgb = b | (g << 8) | (r << 16);
  return '#' + ( 0x1000000 + rgb ).toString( 16 ).slice( 1 );
};