/*!
 * InsaneIntegration v1.1 $insane
 * Copyright 2010-2018 Ricardo Goulart
 * Licensed under the GNU license
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 */

if( $insane == void 0 ){
  throw "Before using $insane, you need to include the insane.boot.js file first.";
}

/*
 ____ ____ ____ ____ ____ ____ ____ 
||S |||T |||R |||I |||N |||G |||S ||
||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.string    = {};

$insane.string.slugify = function( s ){
  if( typeof s == "string" ){
    var rx = new RegExp( "[^a-zA-Z0-9]+", "g" );
    return s.toLowerCase().replace( rx, "-" );
  } else {
    $insane.log( "Provided argument is not a string." );
    return false;
  }
};

$insane.string.reverse = function( s ){
  if( typeof( s ) == "string" ){
    return s.split( "" ).reverse().join( "" );
  } else {
    $insane.log( "Attempt to reverse a string given a non-string argument. #FAILED." );
    return false;
  }
};

/*
 ____ ____ ____ ____ ____ ____ 
||A |||R |||R |||A |||Y |||S ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.array     = {};

$insane.array.shuffle = function( a ){
  if( a instanceof Array ){

    var array_size    = a.length;
    var number_one    = 1;
    var number_zero   = 0;
    var array_random  = 0;
    var array_tmp     = 0;

    if( array_size > 0 ){

      for( var array_counter = array_size - number_one; array_counter > number_zero; array_counter-- ){
        array_random      = Math.floor( Math.random() * ( array_counter + number_one ) );
        array_tmp         = a[array_counter];
        a[array_counter]  = a[array_random];
        a[array_random]   = array_tmp;
      }

    } else {
      throw "It can't shuffle when it has no content!";
    }
  } else {
    throw "It can only shuffle arrays!";
  }
	return a;
};


/*
 ____ ____ ____ ____ ____ ____ ____ 
||C |||O |||O |||K |||I |||E |||S ||
||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.cookie        = {};

$insane.cookie.names  = {};

$insane.cookie.name   = "_ISC_";

  if( window.location.origin !== void 0 ){
    $insane.cookie.name += window.location.origin;
  } else {
    $insane.cookie.name += 'No_browser_support';
  }

$insane.cookie.name += $insane.version;

  if( $insane.encode.md5 !== void 0 ){
    $insane.cookie.name = $insane.encode.md5( $insane.cookie.name );
  }

$insane.cookie._useHTML5 = true;

$insane.cookie.get = function( n ){
  if( localStorage !== void 0 && $insane.cookie._useHTML5 === true ){
    var t = null;
    if( localStorage.getItem( $insane.cookie.name ) !== null ){
      if( t !== void 0 ){
        try {
          t = JSON.parse( localStorage.getItem( $insane.cookie.name ) );
          if( t[n] !== void 0 ){
            return t[n];
          } else {
            $insane.log( "Desired variable does not exist on device stack." );
            return null;
          }
        } catch( e ){
          $insane.log( "There was an error parsing the local variable storage: " + e.message );
          return null;
        }
      } else {
        $insane.log( "No cookie found with given ID." );
        return null;
      }
    } else {
      $insane.log( "No cookie present on device." );
      return null;
    }
  } else {
    var i = 0;
    var y = 0;
    var g = n + "=";
    var c = decodeURIComponent( document.cookie );
    var r = c.split( ';' );
    for(i = 0; i < r.length; i++ ){
      y = r[i];
      while( y.charAt( 0 ) == ' ' ){
        y = y.substring( 1 );
      }
      if( y.indexOf( g ) == 0 ){
        return unescape( y.substring( g.length, y.length ) );
      }
    }
    return null;
  }
};

$insane.cookie.set = function( n, v ){
  if( localStorage !== void 0 && $insane.cookie._useHTML5 === true ){
    var t = null;
    if( localStorage.getItem( $insane.cookie.name ) === null ){
      t = {};
    } else {
      try {
        t = JSON.parse( localStorage.getItem( $insane.cookie.name ) );
        if( t === false ){
          t = {};
        }
      } catch( e ){
        $insane.log( "There was an error parsing the local variable storage: " + e.message );
        return null;
      }
    }
    t[n] = v;
    t = JSON.stringify( t );
    localStorage.setItem( $insane.cookie.name, t );
    return 1;
  } else {
    var d = new Date();
    d.setTime( d.getTime() + ( 365 * 24 * 60 * 60 * 1000 ) );
    var e = "expires=" + d.toUTCString();
    document.cookie = n + "=" + escape( v ) + ";" + e + ";path=/";
  }
};

$insane.cookie.delete = function( n ){
  if( localStorage !== void 0 && $insane.cookie._useHTML5 === true ){
    if( $insane.cookie.get( n ) ){
      t = JSON.parse( localStorage.getItem( $insane.cookie.name ) );
      if( t === false || t === void 0 ){
        return false;
      }
      t[n] = null;
      delete t[n];
      t = JSON.stringify( t );
      localStorage.setItem( $insane.cookie.name, t );
      return 1;
    } else {
      $insane.log( "Cookie var does not exist in device, so it cannot be deleted." );
      return null;
    }
  } else {
    if( $insane.cookie.get( n ) ){
      document.cookie = n + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/";
      return 1;
    } else {
      $insane.log( "Cookie does not exist, so it cannot be deleted." );
      return null;
    }    
  }
};               

/*
 ____ ____ ____ ____ ____ 
||D |||A |||T |||E |||S ||
||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|

*/

$insane.date = {};

$insane.date.parseStringDDMMYYYY = function( s ){
  var d = s || null;
  var x = new RegExp( "\\D" );
  if( d == void 0 ){
    return false;
  } else {
    var r = d.split( x );
    var y = new Date( r[2], --r[1], r[0] );
    return y && y.getMonth() == r[1] ? y : false;
  }
};

$insane.date.parseStringYYYYMMDD = function( s ){
  var d = s || null;
  var x = new RegExp( "\\D" );
  if( d == void 0 ){
    return false;
  } else {
    var r = d.split( x );
    var y = new Date( r[0], --r[1], r[2] );
    return y && y.getMonth() == r[1] ? y : false;
  }
};

$insane.date.parseStringYYYYDDMM = function( s ){
  var d = s || null;
  var x = new RegExp( "\\D" );
  if( d == void 0 ){
    return false;
  } else {
    var r = d.split( x );
    var y = new Date( r[0], --r[2], r[1] );
    return y && y.getMonth() == r[2] ? y : false;
  }
};

$insane.date.parseStringMMDDYYYY = function( s ){
  var d = s || null;
  var x = new RegExp( "\\D" );
  if( d == void 0 ){
    return false;
  } else {
    var r = d.split( x );
    var y = new Date( r[2], --r[0], r[1] );
    return y && y.getMonth() == r[0] ? y : false;
  }
};

$insane.date.getTimeStamp = function( d ){
  if( d instanceof Date ){
    return d.getTime();
  } else {
    throw 'Provided argument is not an instance of Date.';
  }
};