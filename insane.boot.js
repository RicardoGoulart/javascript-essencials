/*!
 * InsaneIntegration v1.1 $insane
 * Copyright 2010-2018 Ricardo Goulart
 * Licensed under the GNU license
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 */

if( $insane == void 0 ){
  var $insane = {};
} else {
  throw "It seems there was an active $insane object instanciated before!";
}

$insane.debug     = 0;

$insane.version   = 1.2;

$insane._validateBrowserConsole = function(){
  if( window.console == void 0 ){
    $insane.debug = 0;
    window.console = function(){
      return;
    };
  } else {
    $insane.debug = !0;
  }
  $insane._validateBrowserConsole = null;
}();

delete $insane._validateBrowserConsole;

$insane.log = function( s ){
  if( typeof s == "string" ){
    window.console.log( s );
  } else {
    return 0;
  }
};


/*
 ____ ____ ____ ____ ____ ____ ____ ____ 
||D |||O |||C |||U |||M |||E |||N |||T ||
||__|||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.document  = {};

$insane.document.ready = function( f ){
  if( f && typeof( f ) == "function" ){
    if( window.jQuery != void 0 ){
      jQuery(document).ready( f );
    } else if( window.$ != void 0 ){
      $(document).ready( f );
    } else {
      if( document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading" ){
        f();
      } else {
        document.addEventListener( 'DOMContentLoaded', f );
      }
    }
	} else {
		if( $insane.debug ){
			throw "$insane.document.ready() must receive a function! it didn't... Failed!";
		}
		return 0;
	}
};

$insane.document.load = function( f ){
  if( f && typeof( f ) == "function" ){
    if( window.jQuery != void 0 ){
      jQuery(document).ready( f );
    } else if( window.$ != void 0 ){
      $(window).load( f );
    } else {
      if( document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading" ){
        f();
      } else {
        document.addEventListener( 'DOMContentLoaded', f );
      }
    }
	} else {
		if( $insane.debug ){
			throw "$insane.document.ready() must receive a function! it didn't... Failed!";
		}
		return 0;
	}
};

$insane.document.resize = function( f ){
  var jQ = null;
  if( window.$ != void 0 ){
    jQ = $;
  } else if( window.jQuery != void 0 ){
    jQ = jQuery;
  }
  
  if( jQ != void 0 ){
    jQ( window ).resize( f );
  } else {
    if( window.addEventListener ){
      window.addEventListener( 'resize', f, false );
    } else if( window.attachEvent ){
      window.attachEvent( 'onresize', f );
    } else {
      window['onresize'] = f;
    }   
  }
};

$insane.document.getXPath = function( e ){
  if( e.tagName == 'HTML' ){
    return '/HTML[1]';
  }
  if( e === document.body ){
    return '/HTML[1]/BODY[1]';
  }

  var ix = 0;
  var siblings = e.parentNode.childNodes;

  for( var i = 0, m = siblings.length; i < m; i++ ){
    var sibling = siblings[i];
    
    if( sibling === e ){
      return $insane.document.getXPath( e.parentNode ) + '/' + e.tagName + '[' + ( ix + 1 ) + ']';
    }

    if( sibling.nodeType === 1 && sibling.tagName === e.tagName ){
      ix++;
    }
  }
};

/*
 ____ ____ ____ ____
||L |||O |||A |||D ||
||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|

*/

$insane.load = {};

$insane.load.active = false;

$insane.load._counts = 0;

$insane.load._scripts = Array();

$insane.load._pending = Array();

$insane.load._readyState = function(){
  var ps = null;
  var sr = document.scripts[0];
  while( $insane.load._pending[0] && $insane.load._pending[0].readyState == 'loaded' ){
    ps = $insane.load._pending.shift();
    ps.onreadystatechange = null;
    sr.parentNode.insertBefore( rs, sr );
  }
};

$insane.load.script = function( u ){
  if( u != void 0 ){
    if( typeof u == "string" ){
      $insane.load.active = true;
      $insane.load._counts++;
      $insane.load._scripts.push( u );
      return true;
    } else {
      throw 'Can only accept string url.';
    }
  } else {
    throw 'Cannot load empty strings.';
  }
};

$insane.load._afterLoad = function(){
  $insane.load._counts--;
};

$insane.load.start = function(){
  /* https://www.html5rocks.com/en/tutorials/speed/script-loading/ */
  if( $insane.load.active === true ){
    $insane.log( "Loading scripts!" );
    var selfReferenceScript = document.scripts[0];
    var script = null;
    var sen = 'script';
    if( !selfReferenceScript ){
      throw 'Could not find the first document ' + sen + '. Weird!';
    }

    while( src = $insane.load._scripts.shift() ){
      if( 'async' in selfReferenceScript ){
        script = document.createElement( sen );
        script.async = false;
        script.onload = $insane.load._afterLoad;
        script.src = src;
        document.head.appendChild( script );
      } else if( selfReferenceScript.readyState ){
        script = document.createElement( sen );
        $insane.load._pending.push( script );
        script.onreadystatechange = $insane.load._readyState;
        script.onload = $insane.load._afterLoad;
        script.src = src;
      } else {
        script = null;
        document.write( '<' + sen + ' onload="java' + sen + ':$insane.load._afterLoad();" type="text/java' + sen + '" src="' + src + '" defer></' + sen + '>' );
      }
    }

    $insane.load.waitForLoad();
  } else {
    return false;
  }
};

$insane.load.waitForLoad = function(){
  if( $insane.load.active === true ){
    try {
      if( $insane.load._counts > 0 ){
        throw new Error( "Waiting for all scripts to load first..." );
      } else {
        for( var i = 0, m = $insane.load._loaded.length; i < m; i++ ){
          $insane.load._loaded[i].call( window );
        }
      }
    } catch( e ){
      setTimeout( $insane.load.waitForLoad, 1000 );
      return;
    }
  } else {
    for( var i = 0, m = $insane.load._loaded.length; i < m; i++ ){
      $insane.load._loaded[i].call( window );
    }
  }
};

$insane.load._loaded = Array();
$insane.load.loaded = function( f ){
  if( typeof f == "function" ){
    $insane.load._loaded.push( f );
  } else {
    throw 'Cannot register function, because is not a function.';
  }
};