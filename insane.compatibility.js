/*!
 * InsaneIntegration v1.1 $insane
 * Copyright 2010-2018 Ricardo Goulart
 * Licensed under the GNU license
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * As additional permission under GNU GPL version 3 section 7, you
 * may distribute non-source (e.g., minimized or compacted) forms of
 * that code without the copy of the GNU GPL normally required by
 * section 4, provided you include this license notice and a URL
 * through which recipients can access the Corresponding Source.   
 */

if( $insane == void 0 ){
  throw "Before using $insane, you need to include the insane.boot.js file first.";
}

/*
 ____ ____ ____ ____ ____ ____ 
||B |||A |||S |||E |||6 |||4 ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|

*/
window['atob'] || window['btoa'] || function( _0x8fe1x1 ){
  var _0x8fe1x4 = '=';
  var _0x8fe1x5 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

  function _0x8fe1x2( _0x8fe1x1, _0x8fe1x2 ){
    var _0x8fe1x3 = _0x8fe1x5[ 'indexOf' ]( _0x8fe1x1[ 'charAt' ]( _0x8fe1x2 ) );

    if( -1 === _0x8fe1x3 ){
      throw 'Unable to decode base64 string'
    };
    return _0x8fe1x3;
  };

  function _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 ){
    var _0x8fe1x3 = _0x8fe1x1[ 'charCodeAt' ]( _0x8fe1x2 );

    if( _0x8fe1x3 > 255 ){
      throw 'There is an invalid character in the base64 string'
    };
    return _0x8fe1x3;
  };

  _0x8fe1x1['btoa'] = function( _0x8fe1x1 ){
    if( 1 !== arguments['length'] ){
      throw 'SyntaxError: exactly one argument required'
    };

    var _0x8fe1x2 = null;
    var _0x8fe1x6 = null;
    var _0x8fe1x7 = [];
    var _0x8fe1x8 = ( _0x8fe1x1 = String( _0x8fe1x1 ) )['length'] - _0x8fe1x1['length'] % 3;

    if( 0 === _0x8fe1x1['length'] ){
      return _0x8fe1x1;
    };

    for( _0x8fe1x2 = 0; _0x8fe1x2 < _0x8fe1x8; _0x8fe1x2 += 3 ){
      _0x8fe1x6 = _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 ) << 16 | _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 + 1 ) << 8 | _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 + 2 ), _0x8fe1x7['push']( _0x8fe1x5['charAt']( _0x8fe1x6 >> 18 ) ), _0x8fe1x7['push']( _0x8fe1x5['charAt']( _0x8fe1x6 >> 12 & 63 ) ), _0x8fe1x7['push']( _0x8fe1x5['charAt']( _0x8fe1x6 >> 6 & 63 ) ), _0x8fe1x7['push']( _0x8fe1x5['charAt']( 63 & _0x8fe1x6 ) );
    };

    switch( _0x8fe1x1['length'] - _0x8fe1x8 ){
      case 1:
        _0x8fe1x6 = _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 ) << 16, _0x8fe1x7['push']( _0x8fe1x5['charAt']( _0x8fe1x6 >> 18 ) + _0x8fe1x5['charAt']( _0x8fe1x6 >> 12 & 63 ) + _0x8fe1x4 + _0x8fe1x4 );
        break;
      case 2:
        _0x8fe1x6 = _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 ) << 16 | _0x8fe1x3( _0x8fe1x1, _0x8fe1x2 + 1 ) << 8, _0x8fe1x7['push']( _0x8fe1x5['charAt']( _0x8fe1x6 >> 18 ) + _0x8fe1x5['charAt']( _0x8fe1x6 >> 12 & 63 ) + _0x8fe1x5['charAt']( _0x8fe1x6 >> 6 & 63 ) + _0x8fe1x4 );
    };
    return _0x8fe1x7['join']('');
  };
  
  _0x8fe1x1['atob'] = function( _0x8fe1x1 ){
    var _0x8fe1x3 = null;
    var _0x8fe1x5 = null;
    var _0x8fe1x6 = 0;
    var _0x8fe1x7 = _0x8fe1x1['length'];
    var _0x8fe1x8 = [];

    if( _0x8fe1x1 = String( _0x8fe1x1 ), 0 === _0x8fe1x7 ){
      return _0x8fe1x1
    };

    if( _0x8fe1x7 % 4 != 0 ){
      throw 'Cannot decode base64';
    };

    for( _0x8fe1x1['charAt']( _0x8fe1x7 - 1 ) === _0x8fe1x4 && ( _0x8fe1x6 = 1, _0x8fe1x1['charAt']( _0x8fe1x7 - 2 ) === _0x8fe1x4 && ( _0x8fe1x6 = 2 ), _0x8fe1x7 -= 4 ), _0x8fe1x3 = 0; _0x8fe1x3 < _0x8fe1x7; _0x8fe1x3 += 4 ){
      _0x8fe1x5 = _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 ) << 18 | _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 + 1 ) << 12 | _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 + 2 ) << 6 | _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 + 3 ), _0x8fe1x8['push']( String[ 'fromCharCode' ]( _0x8fe1x5 >> 16, _0x8fe1x5 >> 8 & 255, 255 & _0x8fe1x5 ) );
    };

    switch( _0x8fe1x6 ){
      case 1:
        _0x8fe1x5 = _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 ) << 18 | _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 + 1 ) << 12 | _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 + 2 ) << 6, _0x8fe1x8[ 'push' ]( String[ 'fromCharCode' ]( _0x8fe1x5 >> 16, _0x8fe1x5 >> 8 & 255 ) );
        break;
      case 2:
        _0x8fe1x5 = _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 ) << 18 | _0x8fe1x2( _0x8fe1x1, _0x8fe1x3 + 1 ) << 12, _0x8fe1x8[ 'push' ]( String[ 'fromCharCode' ]( _0x8fe1x5 >> 16 ) );
    };
    
    return _0x8fe1x8['join']('');
  }
}(window);

/*
 ____ ____ ____ ____ _________ ____ ____ ____ 
||I |||E |||1 |||0 |||       |||F |||I |||X ||
||__|||__|||__|||__|||_______|||__|||__|||__||
|/__\|/__\|/__\|/__\|/_______\|/__\|/__\|/__\|

* IE10 viewport hack for Surface/desktop Windows 8 bug
* Copyright 2014-2018 The Bootstrap Authors
* Copyright 2014-2018 Twitter, Inc.
* Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
*/
var _0x20ef=[ 
  "\x75\x73\x65\x20\x73\x74\x72\x69\x63\x74", 
  "\x6D\x61\x74\x63\x68",
  "\x75\x73\x65\x72\x41\x67\x65\x6E\x74",
  "\x73\x74\x79\x6C\x65",
  "\x63\x72\x65\x61\x74\x65\x45\x6C\x65\x6D\x65\x6E\x74",
  "\x40\x2D\x6D\x73\x2D\x76\x69\x65\x77\x70\x6F\x72\x74\x7B\x77\x69\x64\x74\x68\x3A\x61\x75\x74\x6F\x21\x69\x6D\x70\x6F\x72\x74\x61\x6E\x74\x7D",
  "\x63\x72\x65\x61\x74\x65\x54\x65\x78\x74\x4E\x6F\x64\x65",
  "\x61\x70\x70\x65\x6E\x64\x43\x68\x69\x6C\x64",
  "\x68\x65\x61\x64"
];
(function(){
  _0x20ef[0];
  if( navigator[_0x20ef[2]][_0x20ef[1]](/IEMobile\/10\.0/) ){
    var _0x183fx1 = document[ _0x20ef[4] ]( _0x20ef[3] );
    _0x183fx1[ _0x20ef[7] ]( document[ _0x20ef[6] ]( _0x20ef[5] ) );
    document[ _0x20ef[8] ][ _0x20ef[7] ]( _0x183fx1 )
  }
}());

window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction || {READ_WRITE: "readwrite"};
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
if( !window.indexedDB ){
  $insane.log( "No indexedDB available on the device." );
}

/*
 ____ ____ ____ ____ 
||J |||S |||O |||N ||
||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|

Adds support for json in early browsers
*/
(function(){
  "object"!=typeof JSON&&(JSON={}),function(){"use strict";var rx_one=/^[\],:{}\s]*$/,rx_two=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,rx_three=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,rx_four=/(?:^|:|,)(?:\s*\[)+/g,rx_escapable=/[\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,rx_dangerous=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,gap,indent,meta,rep;function f(t){return t<10?"0"+t:t}function this_value(){return this.valueOf()}function quote(t){return rx_escapable.lastIndex=0,rx_escapable.test(t)?'"'+t.replace(rx_escapable,function(t){var e=meta[t];return"string"==typeof e?e:"\\u"+("0000"+t.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+t+'"'}function str(t,e){var r,n,o,u,f,a=gap,i=e[t];switch(i&&"object"==typeof i&&"function"==typeof i.toJSON&&(i=i.toJSON(t)),"function"==typeof rep&&(i=rep.call(e,t,i)),typeof i){case"string":return quote(i);case"number":return isFinite(i)?String(i):"null";case"boolean":case"null":return String(i);case"object":if(!i)return"null";if(gap+=indent,f=[],"[object Array]"===Object.prototype.toString.apply(i)){for(u=i.length,r=0;r<u;r+=1)f[r]=str(r,i)||"null";return o=0===f.length?"[]":gap?"[\n"+gap+f.join(",\n"+gap)+"\n"+a+"]":"["+f.join(",")+"]",gap=a,o}if(rep&&"object"==typeof rep)for(u=rep.length,r=0;r<u;r+=1)"string"==typeof rep[r]&&(o=str(n=rep[r],i))&&f.push(quote(n)+(gap?": ":":")+o);else for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(o=str(n,i))&&f.push(quote(n)+(gap?": ":":")+o);return o=0===f.length?"{}":gap?"{\n"+gap+f.join(",\n"+gap)+"\n"+a+"}":"{"+f.join(",")+"}",gap=a,o}}"function"!=typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},Boolean.prototype.toJSON=this_value,Number.prototype.toJSON=this_value,String.prototype.toJSON=this_value),"function"!=typeof JSON.stringify&&(meta={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},JSON.stringify=function(t,e,r){var n;if(gap="",indent="","number"==typeof r)for(n=0;n<r;n+=1)indent+=" ";else"string"==typeof r&&(indent=r);if(rep=e,e&&"function"!=typeof e&&("object"!=typeof e||"number"!=typeof e.length))throw new Error("JSON.stringify");return str("",{"":t})}),"function"!=typeof JSON.parse&&(JSON.parse=function(text,reviver){var j;function walk(t,e){var r,n,o=t[e];if(o&&"object"==typeof o)for(r in o)Object.prototype.hasOwnProperty.call(o,r)&&(void 0!==(n=walk(o,r))?o[r]=n:delete o[r]);return reviver.call(t,e,o)}if(text=String(text),rx_dangerous.lastIndex=0,rx_dangerous.test(text)&&(text=text.replace(rx_dangerous,function(t){return"\\u"+("0000"+t.charCodeAt(0).toString(16)).slice(-4)})),rx_one.test(text.replace(rx_two,"@").replace(rx_three,"]").replace(rx_four,"")))return j=eval("("+text+")"),"function"==typeof reviver?walk({"":j},""):j;throw new SyntaxError("JSON.parse")})}();
}());

/*
 ____ ____ ____ ____ ____ ____ ____ ____ 
||E |||N |||C |||O |||D |||E |||R |||S ||
||__|||__|||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|

*/

$insane.encode    = {};

$insane.encode.md5 = function( s ){
  var xl;
  var x = [];
  var k;
  var A; 
  var BB; 
  var CC; 
  var DD; 
  var a; 
  var b; 
  var c; 
  var d; 
  var S11 = 7;
  var S12 = 12;
  var S13 = 17;
  var S14 = 22;
  var S21 = 5;
  var S22 = 9;
  var S23 = 14;
  var S24 = 20;
  var S31 = 4;
  var S32 = 11;
  var S33 = 16;
  var S34 = 23;
  var S41 = 6;
  var S42 = 10;
  var S43 = 15;
  var S44 = 21;

  var rotateLeft = function( leftValue, iShiftBits ){
    return ( leftValue << iShiftBits ) | ( leftValue >>> ( 32 - iShiftBits ) );
  };

  var addUnsigned = function( lX, lY ){
    var lX4;
    var lY4;
    var lX8;
    var lY8;
    var lR;

    lX8 = (lX & 0x80000000);
    lY8 = (lY & 0x80000000);
    lX4 = (lX & 0x40000000);
    lY4 = (lY & 0x40000000);
    
    lR = ( lX & 0x3FFFFFFF ) + ( lY & 0x3FFFFFFF );
    
    if( lX4 & lY4 ){
      return ( lR ^ 0x80000000 ^ lX8 ^ lY8 );
    }

    if( lX4 | lY4 ){
      if( lR & 0x40000000 ){
        return ( lR ^ 0xC0000000 ^ lX8 ^ lY8 );
      } else {
        return ( lR ^ 0x40000000 ^ lX8 ^ lY8 );
      }
    } else {
      return ( lR ^ lX8 ^ lY8 );
    }
  };

  var _F = function( x, y, z ){
    return ( x & y ) | ( ( ~x ) & z );
  };

  var _G = function( x, y, z ){
    return ( x & z ) | ( y & ( ~z ) );
  };

  var _H = function( x, y, z ){
    return ( x ^ y ^ z );
  };

  var _I = function( x, y, z ){
    return ( y ^ ( x | ( ~z ) ) );
  };

  var _FF = function( a, b, c, d, x, s, ac ){
    a = addUnsigned( a, addUnsigned( addUnsigned( _F( b, c, d ), x ), ac ) );
    return addUnsigned( rotateLeft( a, s ), b );
  };

  var _GG = function( a, b, c, d, x, s, ac ){
    a = addUnsigned( a, addUnsigned( addUnsigned( _G( b, c, d ), x ), ac ) );
    return addUnsigned( rotateLeft( a, s ), b );
  };

  var _HH = function( a, b, c, d, x, s, ac ){
    a = addUnsigned( a, addUnsigned( addUnsigned( _H( b, c, d ), x ), ac ) );
    return addUnsigned( rotateLeft( a, s ), b );
  };

  var _II = function( a, b, c, d, x, s, ac ){
    a = addUnsigned( a, addUnsigned( addUnsigned( _I( b, c, d ), x ), ac ) );
    return addUnsigned( rotateLeft( a, s ), b );
  };

  var convertToWordArray = function( str ){
    var lWordCount;
    var lMessageLength = str.length;
    var lNumberOfWords_temp1 = lMessageLength + 8;
    var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
    var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
    var lWordArray = new Array(lNumberOfWords - 1);
    var lBytePosition = 0;
    var lByteCount = 0;
    while (lByteCount < lMessageLength) {
      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
      lBytePosition = (lByteCount % 4) * 8;
      lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
      lByteCount++;
    }
    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
    lBytePosition = (lByteCount % 4) * 8;
    lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
    lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
    return lWordArray;
  };

  var wordToHex = function(lValue) {
    var wordToHexValue = '';
    var wordToHexValue_temp = '';
    var lByte;
    var lCount;

    for( lCount = 0; lCount <= 3; lCount++ ){
      lByte = ( lValue >>> ( lCount * 8 ) ) & 255;
      wordToHexValue_temp = '0' + lByte.toString( 16 );
      wordToHexValue = wordToHexValue + wordToHexValue_temp.substr( wordToHexValue_temp.length - 2, 2 );
    }

    return wordToHexValue;
  };

  var _md5_utf8 = function( s ){
    var regExp1 = new RegExp( "\\r\\n", "g" );
    var regExp2 = new RegExp( "\\r", "g" );
    var string = ( s + '' ).replace( regExp1, "\n" ).replace( regExp2, "\n" );
    var utftext = "";
    var start;
    var end;
    var n = 0;
    var stringl = 0;
    var c1 = null;
    var enc = null;
    start = end = 0;
    stringl = string.length;
  
    for( n = 0; n < stringl; n++ ){
      c1 = string.charCodeAt( n );
      enc = null;
  
      if( c1 < 128 ){
        end++;
      } else if( ( c1 > 127 ) && ( c1 < 2048 ) ){
        enc = String.fromCharCode( ( c1 >> 6 ) | 192 ) + String.fromCharCode( ( c1 & 63 ) | 128 );
      } else {
        enc = String.fromCharCode( ( c1 >> 12 ) | 224 ) + String.fromCharCode( ( ( c1 >> 6 ) & 63 ) | 128 ) + String.fromCharCode( ( c1 & 63 ) | 128 );
      }
      
      if( enc != null ){
        if( end > start ){
          utftext += string.substring( start, end );
        }
        utftext += enc;
        start = end = n+1;
      }
    }
  
    if( end > start ){
      utftext += string.substring( start, string.length );
    }
  
    return utftext;
  };

  str = _md5_utf8( s );
  x = convertToWordArray( str );
  a = 0x67452301;
  b = 0xEFCDAB89;
  c = 0x98BADCFE;
  d = 0x10325476;
  xl = x.length;

  for( k = 0; k < xl; k += 16 ){
    AA = a;
    BB = b;
    CC = c;
    DD = d;
    a = _FF( a, b, c, d, x[k + 0], S11, 0xD76AA478 );
    d = _FF( d, a, b, c, x[k + 1], S12, 0xE8C7B756 );
    c = _FF( c, d, a, b, x[k + 2], S13, 0x242070DB );
    b = _FF( b, c, d, a, x[k + 3], S14, 0xC1BDCEEE );
    a = _FF( a, b, c, d, x[k + 4], S11, 0xF57C0FAF );
    d = _FF( d, a, b, c, x[k + 5], S12, 0x4787C62A );
    c = _FF( c, d, a, b, x[k + 6], S13, 0xA8304613 );
    b = _FF( b, c, d, a, x[k + 7], S14, 0xFD469501 );
    a = _FF( a, b, c, d, x[k + 8], S11, 0x698098D8 );
    d = _FF( d, a, b, c, x[k + 9], S12, 0x8B44F7AF );
    c = _FF( c, d, a, b, x[k + 10], S13, 0xFFFF5BB1 );
    b = _FF( b, c, d, a, x[k + 11], S14, 0x895CD7BE );
    a = _FF( a, b, c, d, x[k + 12], S11, 0x6B901122 );
    d = _FF( d, a, b, c, x[k + 13], S12, 0xFD987193 );
    c = _FF( c, d, a, b, x[k + 14], S13, 0xA679438E );
    b = _FF( b, c, d, a, x[k + 15], S14, 0x49B40821 );
    a = _GG( a, b, c, d, x[k + 1], S21, 0xF61E2562 );
    d = _GG( d, a, b, c, x[k + 6], S22, 0xC040B340 );
    c = _GG( c, d, a, b, x[k + 11], S23, 0x265E5A51 );
    b = _GG( b, c, d, a, x[k + 0], S24, 0xE9B6C7AA );
    a = _GG( a, b, c, d, x[k + 5], S21, 0xD62F105D );
    d = _GG( d, a, b, c, x[k + 10], S22, 0x2441453 );
    c = _GG( c, d, a, b, x[k + 15], S23, 0xD8A1E681 );
    b = _GG( b, c, d, a, x[k + 4], S24, 0xE7D3FBC8 );
    a = _GG( a, b, c, d, x[k + 9], S21, 0x21E1CDE6 );
    d = _GG( d, a, b, c, x[k + 14], S22, 0xC33707D6 );
    c = _GG( c, d, a, b, x[k + 3], S23, 0xF4D50D87 );
    b = _GG( b, c, d, a, x[k + 8], S24, 0x455A14ED );
    a = _GG( a, b, c, d, x[k + 13], S21, 0xA9E3E905 );
    d = _GG( d, a, b, c, x[k + 2], S22, 0xFCEFA3F8 );
    c = _GG( c, d, a, b, x[k + 7], S23, 0x676F02D9 );
    b = _GG( b, c, d, a, x[k + 12], S24, 0x8D2A4C8A );
    a = _HH( a, b, c, d, x[k + 5], S31, 0xFFFA3942 );
    d = _HH( d, a, b, c, x[k + 8], S32, 0x8771F681 );
    c = _HH( c, d, a, b, x[k + 11], S33, 0x6D9D6122 );
    b = _HH( b, c, d, a, x[k + 14], S34, 0xFDE5380C );
    a = _HH( a, b, c, d, x[k + 1], S31, 0xA4BEEA44 );
    d = _HH( d, a, b, c, x[k + 4], S32, 0x4BDECFA9 );
    c = _HH( c, d, a, b, x[k + 7], S33, 0xF6BB4B60 );
    b = _HH( b, c, d, a, x[k + 10], S34, 0xBEBFBC70 );
    a = _HH( a, b, c, d, x[k + 13], S31, 0x289B7EC6 );
    d = _HH( d, a, b, c, x[k + 0], S32, 0xEAA127FA );
    c = _HH( c, d, a, b, x[k + 3], S33, 0xD4EF3085 );
    b = _HH( b, c, d, a, x[k + 6], S34, 0x4881D05 );
    a = _HH( a, b, c, d, x[k + 9], S31, 0xD9D4D039 );
    d = _HH( d, a, b, c, x[k + 12], S32, 0xE6DB99E5 );
    c = _HH( c, d, a, b, x[k + 15], S33, 0x1FA27CF8 );
    b = _HH( b, c, d, a, x[k + 2], S34, 0xC4AC5665 );
    a = _II( a, b, c, d, x[k + 0], S41, 0xF4292244 );
    d = _II( d, a, b, c, x[k + 7], S42, 0x432AFF97 );
    c = _II( c, d, a, b, x[k + 14], S43, 0xAB9423A7 );
    b = _II( b, c, d, a, x[k + 5], S44, 0xFC93A039 );
    a = _II( a, b, c, d, x[k + 12], S41, 0x655B59C3 );
    d = _II( d, a, b, c, x[k + 3], S42, 0x8F0CCC92 );
    c = _II( c, d, a, b, x[k + 10], S43, 0xFFEFF47D );
    b = _II( b, c, d, a, x[k + 1], S44, 0x85845DD1 );
    a = _II( a, b, c, d, x[k + 8], S41, 0x6FA87E4F );
    d = _II( d, a, b, c, x[k + 15], S42, 0xFE2CE6E0 );
    c = _II( c, d, a, b, x[k + 6], S43, 0xA3014314 );
    b = _II( b, c, d, a, x[k + 13], S44, 0x4E0811A1 );
    a = _II( a, b, c, d, x[k + 4], S41, 0xF7537E82 );
    d = _II( d, a, b, c, x[k + 11], S42, 0xBD3AF235 );
    c = _II( c, d, a, b, x[k + 2], S43, 0x2AD7D2BB );
    b = _II( b, c, d, a, x[k + 9], S44, 0xEB86D391 );
    a = addUnsigned( a, AA );
    b = addUnsigned( b, BB );
    c = addUnsigned( c, CC );
    d = addUnsigned( d, DD );
  }

  var temp = wordToHex( a ) + wordToHex( b ) + wordToHex( c ) + wordToHex( d );
  return temp.toLowerCase();
};

$insane.encode.insane = function( n ){
  if( n != void 0 ){
    var jsonStringified = JSON.stringify( n );
    var jsonB64 = window.btoa( jsonStringified );
    var jsonReversed = $insane.string.reverse( jsonB64 );
    return jsonReversed;
  } else {
    $insane.log( "Cannot encode empty argument." );
    return false;
  }
};