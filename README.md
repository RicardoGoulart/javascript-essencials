# Javascript Essencials 1.1 #

This repository serves as a big helper for web/javascript projects. Pull everything, and use only what you need.

### TODO ###
* Make possible to lighten or darken a color regardless of it's TYPE (RGB or HEX)
* Make $insane.document.resize jQuery FREE (independent)
* Check other code from ESRI app

## What is the Load order? (Version => 1.4 ) ##
1. insane-1.4-boot.min.js (do it async!)
```javascript
$insane.document.ready(function(){
  $insane.load.script( '//example.com/jquery.js' );
  $insane.load.script( '//example.com/bootstrap.js' );
  $insane.load.script( '//example.com/fontawesome.js' );
  
  $insane.load.script( '//example.com/insane-1.4-components.min.js' );
  $insane.load.start();
});
```

## What is the Load order? (Version <= 1.3 ) ##
1. insane.essentials.min.js (jQuery + Thether + Popper + Bootstrap 4 + Fontawesome 5 )
2. insane.boot.js ( !Mandatory! )
3. insane.compatibility.js
4. insane.javascript.essencials.js
5. insane.javascript.color.js
6. insane.javascript.ajax.js
7. insane.javascript.image.js

## What is in the repository? ##
* Window BTOA & ATOB functions
* Javascript array shuffle function
* IE navigator fix
* Document ready function (Falls back to jQuery if available)
* Color functions to lighten or darken a color
* Convert HEX to RGB or vice-versa
* Save, get and delete a variable on the device (localstorage with fallback to cookie)
* MD5 crypt function
* Ajax requests
* Script loading management

## Change Log ##

### Version 1.4 ###
* Segmented for best performance.
* Added ajax requests

### Version 1.3 ###
* Added $insane.document.getXPath
* Some changes to $insane.image

### Version 1.2 ###
* Segmented the code in files
* Added $insane.image functions
* Added $insane.document.resize support
* Added $insane.string.slugify wich removes spaces and other chars than 0-9a-zA-Z
* Added $insane.string.reverse to reverse a string
* Added insane.essentials.min.js file
* Added BETA version of INSANE-IMAGE (might be working)

### Version 1.1 ###
* Added cookie GET, SET, DELETE functions
* Added MD5 Crypt function
* Added function to convert RGB colors to HEX
* Added function to convert HEX to RGB
* Added function to lighten a color (in HEX only)
* Added function to darken a color (in HEX only)

### Version 1.0 ###
* Separated the contents of the file into multiple files
* Cleansed the files of inline comments so you can easy minify them
* Added minified version of all files

### Version 0.1 ###
* Added functions to encode and decode base64 strings
* Added a javascript function to shuffle an array
* Added IE navigator fix

#### Who do I talk to? ####
* Ricardo Goulart <ricardo@goulart.pt>